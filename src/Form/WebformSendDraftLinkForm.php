<?php

namespace Drupal\webform_send_draft_link\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\WebformInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;

/**
 * Sends a webform draft link via email.
 */
class WebformSendDraftLinkForm extends ConfigFormBase {
  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The url of the link to a draft.
   *
   * @var string
   */
  protected $tokenUrl;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_send_draft_link.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_send_draft_link_form';
  }

  /**
   * Hide "Send draft link" tab if a webform doesn't have the setting for this.
   *
   * We read getThirdPartySetting('webform_send_draft_link', 'enabled') from a
   * webform and if a value is NULL or FALSE then we deny access to the route of
   * this (WebformSendDraftLinkForm) form. And if access to the route is denied,
   * then the "Send draft link" tab (Local task) is not display on the webform.
   *
   * @param Drupal\webform\WebformInterface $webform
   *   The current webform.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(WebformInterface $webform) {
    $route_name = $this->routeMatch->getRouteName();

    // Prevent to display "Send draft link" on all pages except of
    // "View webform", "View submission", "Edit submission", "Notes submission".
    if ($route_name !== 'entity.webform.canonical'
       && $route_name !== 'entity.webform_submission.canonical'
       && $route_name !== 'entity.webform_submission.edit_form'
       && $route_name !== 'entity.webform_submission.notes_form'
       && $route_name !== 'webform_send_draft_link.send_email_form.webform.canonical'
       && $route_name !== 'webform_send_draft_link.send_email_form.webform_submission.canonical'
       && $route_name !== 'webform_send_draft_link.send_email_form.webform_submission.edit_form'
       && $route_name !== 'webform_send_draft_link.send_email_form.webform_submission.notes_form') {

      return AccessResult::forbidden();
    }

    return AccessResult::allowedIf($webform->getThirdPartySetting('webform_send_draft_link', 'enabled'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL) {
    if (!$webform->getSetting('token_update')) {
      $this->messenger()->addWarning($this->t('The "Allow users to update a submission using a secure token" option is disabled.'));
      return $form;
    }

    $webform_id = $webform->id();
    $settings = $this->state->get('webform_send_draft_link_settings');

    if (!isset($settings[$webform_id]['token_url'])) {
      $this->messenger()->addWarning($this->t('The draft has not been saved yet.'));
      return $form;
    }

    $this->tokenUrl = $settings[$webform_id]['token_url'];
    $is_completed = $settings[$webform_id]['is_completed'];

    if ($is_completed) {
      $this->messenger()->addWarning($this->t('The submission has been already completed.'));
      return $form;
    }

    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#required' => TRUE,
      '#description' => $this->t("It's possible to input several emails separated by comma."),
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message (the link will be attached automatically)'),
      '#rows' => 3,
    ];

    $form['link'] = [
      '#type' => 'item',
      '#markup' => '<a href="' . $this->tokenUrl . '">' . $this->tokenUrl . '</a>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $to_emails = Html::escape($form_state->getValue('to'));
    $to_emails = explode(',', $to_emails);

    foreach ($to_emails as $to_email) {

      $email = trim($to_email);

      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $form_state->setErrorByName('to', $this->t('@email is an invalid email address.', ['@email' => $to_email]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $from = $this->configFactory->get('system.site')->get('mail');
    $subject = Html::escape($form_state->getValue('subject'));
    $message = Xss::filterAdmin($form_state->getValue('message'));
    $link = '<a href="' . $this->tokenUrl . '">' . $this->tokenUrl . '</a>';
    $body = $message . '<br>' . $link;

    $to_emails = Html::escape($form_state->getValue('to'));
    $to_emails = explode(',', $to_emails);

    foreach ($to_emails as $to_email) {
      $to = trim($to_email);
      simple_mail_send($from, $to, $subject, $body);
    }

    $this->messenger()->addStatus($this->t('The message was sent.'));
  }

}
